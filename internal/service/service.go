package service

import (
	"mysql/internal/model"
	"mysql/internal/repository"
)

type ServiceInterface interface {
	Create() error
	Read() ([]model.User, error)
	Update(model.User) error
	Delete(int) error
}
type Service struct {
	repository repository.RepositoryInterface
}

func NewService(repository repository.RepositoryInterface) *Service {
	return &Service{repository: repository}
}

func (s *Service) Create() error {
	return s.repository.Create()
}

func (s *Service) Read() ([]model.User, error) {
	return s.repository.Read()
}

func (s *Service) Update(user model.User) error {
	return s.repository.Update(user)
}

func (s *Service) Delete(id int) error {
	return s.repository.Delete(id)
}
