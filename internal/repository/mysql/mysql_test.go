package mysql

import (
	"mysql/internal/model"
	"testing"
)

func TestCreateRead(t *testing.T) {
	db, err := NewMySQLDB()
	if err != nil {
		t.Fatalf("failed to create MySQLDB: %v", err)
	}

	//удаляем все данные из бд
	err = db.DeleteAll()
	if err != nil {
		t.Fatalf("failed to delete all records: %v", err)
	}

	//проверяем методы CREATE И READ
	data := model.Data{
		ID:    1,
		Name:  "Test",
		Value: 100,
	}

	err = db.Create(data)
	if err != nil {
		t.Fatalf("failed to create data: %v", err)
	}

	// Проверяем, что запись была успешно создана
	datas, err := db.Read()
	if err != nil {
		t.Fatalf("failed to read data: %v", err)
	}
	if len(datas) != 1 {
		t.Fatalf("expected 1 data, got %d", len(datas))
	}

	//проверяем соответствие данных
	if datas[0].Name != data.Name {
		t.Errorf("expected name %s, got %s", data.Name, datas[0].Name)
	}
	if datas[0].Value != data.Value {
		t.Errorf("expected value %d, got %d", data.Value, datas[0].Value)
	}

	//проверяем метод UPDATE
	newData := model.Data{
		ID:    1,
		Name:  "NewTest",
		Value: 10000,
	}

	err = db.Update(newData)
	if err != nil {
		t.Fatalf("failed to update data: %v", err)
	}

	//проверяем соответствие данных
	datas, err = db.Read()
	if err != nil {
		t.Fatalf("failed to read data: %v", err)
	}

	if datas[0].Name != newData.Name {
		t.Errorf("expected name %s, got %s", newData.Name, datas[0].Name)
	}
	if datas[0].Value != newData.Value {
		t.Errorf("expected value %d, got %d", newData.Value, datas[0].Value)
	}

	//проверяем метод DELETE
	err = db.Delete(1)
	if err != nil {
		t.Fatalf("failed to delete data: %v", err)
	}

	datas, err = db.Read()
	if err != nil {
		t.Fatalf("failed to read data: %v", err)
	}
	if len(datas) != 0 {
		t.Fatalf("expected 0 data, got %d", len(datas))
	}
}

func (m *MySQLDB) DeleteAll() error {
	_, err := m.db.Exec("DELETE FROM data")
	if err != nil {
		return err
	}
	// обнуляем счетчик
	_, err = m.db.Exec("ALTER TABLE data AUTO_INCREMENT = 1")
	return err
}
