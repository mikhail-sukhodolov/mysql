package mysql

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"mysql/internal/model"
)

type MySQLDB struct {
	db *sqlx.DB
}

func NewMySQLDB() (*MySQLDB, error) {

	connStr := "root:12345@tcp(localhost:3306)/mysql"
	db, err := sqlx.Open("mysql", connStr)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	//создаем таблицу, если не существует
	query := `
  CREATE TABLE IF NOT EXISTS Users (
 		id INT PRIMARY KEY,
		first_name VARCHAR(50),
		last_name VARCHAR(50),
		age INT,
		email VARCHAR(100),
		phone VARCHAR(20),
		address VARCHAR(100),
		city VARCHAR(50),
		country VARCHAR(50)
	);
`

	if _, err := db.Exec(query); err != nil {
		return nil, err
	}
	return &MySQLDB{db: db}, nil
}

func (m *MySQLDB) Create() error {
	_, err := m.db.Exec("INSERT INTO Users (id, first_name, last_name, age, email, phone, address, city, country) VALUES " +
		"(1, 'John', 'Doe', 25, 'johndoe@example.com', '123-456-7890', '123 Main St', 'New York', 'USA')," +
		"(2, 'Jane', 'Smith', 32, 'janesmith@example.com', '987-654-3210', '456 Elm St', 'Los Angeles', 'USA')," +
		"(3, 'Mike', 'Johnson', 41, 'mikejohnson@example.com', '555-123-4567', '789 Oak Ave', 'Chicago', 'USA')," +
		"(4, 'Emily', 'Brown', 29, 'emilybrown@example.com', '111-222-3333', '321 Pine Rd', 'London', 'UK')," +
		"(5, 'Alex', 'Lee', 37, 'alexlee@example.com', '444-555-6666', '567 Maple Dr', 'Toronto', 'Canada');")
	if err != nil {
		return err
	}
	return nil
}

func (m *MySQLDB) Read() ([]model.User, error) {
	var users []model.User
	if err := m.db.Select(&users, "SELECT * FROM Users"); err != nil {
		return nil, err
	}
	return users, nil

}

func (m *MySQLDB) Update(user model.User) error {
	// проверяем наличие
	var tempUser model.User
	if err := m.db.Get(&tempUser, "SELECT * FROM Users WHERE id=?", user.Id); err != nil {
		return fmt.Errorf("no such user")
	}
	// делаем апдейт
	if _, err := m.db.Exec("UPDATE Users SET first_name=?, last_name=?, age=?, email=? WHERE id=?", user.First_name, user.Last_name, user.Age, user.Email, user.Id); err != nil {
		return err
	}
	return nil

}

func (m *MySQLDB) Delete(id int) error {
	if _, err := m.db.Exec("DELETE FROM Users WHERE id=?", id); err != nil {
		return err
	}
	return nil
}
