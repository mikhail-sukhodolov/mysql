package repository

import (
	"mysql/internal/model"
)

type RepositoryInterface interface {
	Create() error
	Read() ([]model.User, error)
	Update(model.User) error
	Delete(int) error
}
