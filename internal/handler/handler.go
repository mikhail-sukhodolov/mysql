package handler

import (
	"github.com/gin-gonic/gin"
	"mysql/internal/model"
	"mysql/internal/service"
	"net/http"
	"strconv"
)

type Handler struct {
	service service.ServiceInterface
}

func NewHandler(service service.ServiceInterface) *Handler {
	return &Handler{service: service}
}

// Create
// @Summary      Create
// @Description  Add new data
// @Tags         CRUD
// @Accept       json
// @Produce      json
// @Param   input   body   model.Data   true   "JSON object"
// @Failure      400
// @Failure      500
// @Succes		 200
// @Router       /create [post]
func (h *Handler) Create(c *gin.Context) {
	/*var data model.Data
	err := c.BindJSON(&data)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}*/
	err := h.service.Create()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)
}

// Read
// @Summary      Read
// @Description  Data list
// @Tags         CRUD
// @Accept       json
// @Produce      json
// @Failure      400
// @Failure      500
// @Success      200 {object} []model.Data
// @Router       /read [get]
func (h *Handler) Read(c *gin.Context) {
	datas, err := h.service.Read()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
	}

	c.JSON(http.StatusOK, datas)

}

// Update
// @Summary      Update
// @Description  Update data
// @Tags         CRUD
// @Accept       json
// @Produce      json
// @Param   input   body   model.Data   true   "JSON object"
// @Failure      400
// @Failure      500
// @Succes		 200
// @Router       /update [put]
func (h *Handler) Update(c *gin.Context) {
	var user model.User
	err := c.BindJSON(&user)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.Update(user)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)
}

// Delete
// @Summary      Delete
// @Description  Delete data
// @Tags         CRUD
// @Param id query int true "ID"
// @Accept       json
// @Produce      json
// @Failure      400
// @Failure      500
// @Success      200
// @Router       /delete [delete]
func (h *Handler) Delete(c *gin.Context) {
	id, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.Delete(id)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)
}
