package handler

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "mysql/docs"
)

func (h *Handler) InitRoutes() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.POST("/create", h.Create)
	r.GET("/read", h.Read)
	r.PUT("/update", h.Update)
	r.DELETE("/delete", h.Delete)
	return r
}
