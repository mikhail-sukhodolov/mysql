package model

type Data struct {
	ID    int    `json:"id" db:"id"`
	Name  string `json:"name" db:"name"`
	Value int    `json:"value" db:"value"`
}

type User struct {
	Id         int    `json:"id" db:"id"`
	First_name string `json:"first_name" db:"first_name"`
	Last_name  string `json:"last_name" db:"last_name"`
	Age        int    `json:"age" db:"age"`
	Email      string `json:"email" db:"email"`
	Phone      string `json:"phone" db:"phone"`
	Address    string `json:"address" db:"address"`
	City       string `json:"city" db:"city"`
	Country    string `json:"country" db:"country"`
}
