package main

import (
	"context"
	"github.com/rs/zerolog/log"
	"mysql/internal/handler"
	"mysql/internal/repository/mysql"
	"mysql/internal/service"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {

	// @title           MySQL CRUD example
	// @version         1.0
	// @description     API server for realize mysql CRUD
	// @host     	    mysql

	repo, err := mysql.NewMySQLDB()
	if err != nil {
		log.Fatal().Err(err).Msg("Error database create")
	}
	serv := service.NewService(repo)
	hand := handler.NewHandler(serv)

	port := ":8080"
	server := http.Server{
		Addr:         port,
		Handler:      hand.InitRoutes(),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	// запускаем сервер
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("fatal error starting server")
		}
	}()

	log.Info().Msg("Starting server")
	// ожидаем сигнал остановки
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	// оcтаналиваем сервер
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Err(err).Msg("shutdown server error")
	}
	defer server.Close()
}
